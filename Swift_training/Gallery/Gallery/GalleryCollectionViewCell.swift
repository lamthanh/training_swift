//
//  GalleryCollectionViewCell.swift
//  Gallery
//
//  Created by Thanh Lam on 2/9/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgvGallery: UIImageView!
}
