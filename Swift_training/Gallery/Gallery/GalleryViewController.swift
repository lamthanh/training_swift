//
//  GalleryViewController.swift
//  Gallery
//
//  Created by Thanh Lam on 2/9/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit
import Photos

class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate {
    
    @IBOutlet weak var collvGallery: UICollectionView!
    
    var imageArr = [UIImage]()
    var passedContentOffset = IndexPath()
    var indicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Photos"
        
        self.collvGallery.delegate = self
        self.collvGallery.dataSource = self
        
        self.collvGallery.allowsMultipleSelection = true
        
        //Create indicator
        indicatorView.center = self.view.center
        indicatorView.hidesWhenStopped = true
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        //Edit layout cell
        let itemSize = UIScreen.main.bounds.width/3 - 3
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(20, 0, 10, 0)
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        layout.minimumLineSpacing = 3
        layout.minimumInteritemSpacing = 3
        
        collvGallery.collectionViewLayout = layout

        // Do any additional setup after loading the view.
        collectImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        indicatorView.startAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnSelect(_ sender: Any) {
        if navigationItem.rightBarButtonItem?.title == "Select" {
            navigationItem.rightBarButtonItem?.title = "Cancel"
            return
        } else if navigationItem.rightBarButtonItem?.title == "Cancel" {
            navigationItem.rightBarButtonItem?.title = "Select"
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GalleryCollectionViewCell
        cell.imgvGallery.image = imageArr[indexPath.item]
        indicatorView.stopAnimating()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        passedContentOffset = indexPath
        performSegue(withIdentifier: "Preview_Segue", sender: self)
    }
    
    //Transfer array, position to destination VC
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Preview_Segue" {
            let prVC = segue.destination as! PreviewViewController
            prVC.imgArr = self.imageArr
            prVC.passedContentOffset = self.passedContentOffset
        }
    }
    
    // MARK: Get image from system gallery
    func collectImage() {
        self.imageArr = []
        
        DispatchQueue.global(qos: .background).async {
            let imgManager = PHImageManager.default()
            
            //Collecting image from system gallery
            let requestOptions = PHImageRequestOptions()
            requestOptions.isSynchronous = true
            requestOptions.deliveryMode = .highQualityFormat
            
            //Order by Date
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
            
            //Append image to imageArray
            let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            print(fetchResult)
            print(fetchResult.count)
            if fetchResult.count > 0 {
                for i in 0..<fetchResult.count{
                    imgManager.requestImage(for: fetchResult.object(at: i) as PHAsset, targetSize: CGSize(width:500, height: 500),contentMode: .aspectFill, options: requestOptions, resultHandler: { (image, error) in
                        self.imageArr.append(image!)
                    })
                }
            } else {
                print("No photo.")
            }
            print("imageArray count: \(self.imageArr.count)")
            
            //Reload collection view
            DispatchQueue.main.async {
                print("Main queue")
                self.collvGallery.reloadData()
            }
        }
    }

}

struct DeviceInfo {
    struct Orientation {
        // indicate current device is in the LandScape orientation
        static var isLandscape: Bool {
            get {
                return UIDevice.current.orientation.isValidInterfaceOrientation
                    ? UIDevice.current.orientation.isLandscape
                    : UIApplication.shared.statusBarOrientation.isLandscape
            }
        }
        // indicate current device is in the Portrait orientation
        static var isPortrait: Bool {
            get {
                return UIDevice.current.orientation.isValidInterfaceOrientation
                    ? UIDevice.current.orientation.isPortrait
                    : UIApplication.shared.statusBarOrientation.isPortrait
            }
        }
    }
}
