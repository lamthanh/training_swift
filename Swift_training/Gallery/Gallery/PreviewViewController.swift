//
//  PreviewViewController.swift
//  Gallery
//
//  Created by Thanh Lam on 2/9/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrPreview: UIScrollView!
    @IBOutlet weak var imgvPreview: UIImageView!
    
    var imgArr = [UIImage]()
    var passedContentOffset = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrPreview.delegate = self
        
        self.imgvPreview.image = imgArr[passedContentOffset.row]
        
        //Zoom when double click on image
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrPreview.addGestureRecognizer(doubleTapGest)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //Scrollview delegate func
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgvPreview
    }
    
    //Double tap reconizer
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if scrPreview.zoomScale == 1 {
            scrPreview.zoom(to: zoomRectForScale(scale: scrPreview.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            scrPreview.setZoomScale(1, animated: true)
        }
    }
    
    //Zoom in - zoom out, get center image when double tap
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imgvPreview.frame.size.height / scale
        zoomRect.size.width  = imgvPreview.frame.size.width  / scale
        let newCenter = imgvPreview.convert(center, from: scrPreview)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }

}
