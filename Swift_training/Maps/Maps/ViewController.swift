//
//  ViewController.swift
//  Maps
//
//  Created by Thanh Lam on 2/26/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ViewController: UIViewController, CLLocationManagerDelegate, UISearchBarDelegate, LocateOnTheMap {
    
    @IBOutlet weak var mapView: GMSMapView!
    let manager = CLLocationManager()
    
    var searchResultController: Results_TableViewController!
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Show map controll
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        
        manager.delegate = self
        //Setting up location manager
        manager.desiredAccuracy = kCLLocationAccuracyBest
        //Request permission to use location
        manager.requestWhenInUseAuthorization()
        //Start update location
        manager.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        searchResultController = Results_TableViewController()
        searchResultController.delegate = self
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //CLLocationManager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        showMap(latitude: location.coordinate.latitude, longtitude: location.coordinate.longitude)
    }
    
    func showMap(latitude: CLLocationDegrees, longtitude: CLLocationDegrees) {
        //        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longtitude, zoom: 17.5)
        //        mapView.camera = camera
    }
    
    //Show result location
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        DispatchQueue.main.async { () -> Void in
            
            let position = CLLocationCoordinate2DMake(lat, lon)
            let marker = GMSMarker(position: position)
            
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 17.5)
            self.mapView.camera = camera
            
            marker.title = "Address : \(title)"
            marker.map = self.mapView
            
        }
    }
    
    //Clear resultsArray and re-fetch when change text
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.resultsArray.removeAll()
        gmsFetcher.sourceTextHasChanged(searchText)
    }
    
    //Show search bar
    @IBAction func searchWithAddress(_ sender: Any) {
        let searchCotroller = UISearchController(searchResultsController: searchResultController)
        searchCotroller.searchBar.delegate = self
        self.present(searchCotroller, animated: true, completion: nil)
    }
}

extension ViewController: GMSAutocompleteFetcherDelegate {
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        //Append all prediction to resultsArray
        for prediction in predictions {
            if let prediction = prediction as GMSAutocompletePrediction!{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        //Show resultsArray on tableview
        self.searchResultController.reloadDataWithArray(self.resultsArray)
        print(resultsArray)
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        return
    }
}



