//
//  GalleryCollectionViewCell.swift
//  MediaManager
//
//  Created by Thanh Lam on 2/12/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgViewCell: UIImageView!
    
}
