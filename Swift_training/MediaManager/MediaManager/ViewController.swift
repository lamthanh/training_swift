//
//  ViewController.swift
//  MediaManager
//
//  Created by Thanh Lam on 2/12/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit
import Photos

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate {
    
    @IBOutlet weak var collGallery: UICollectionView!
    
    var imageArr = [UIImage]()
    var imgSelectedArr: Dictionary = [Int: UIImage]()
    var num = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Photos"
        
        self.collGallery.delegate = self
        self.collGallery.dataSource = self
        
        self.collGallery.allowsMultipleSelection = true
        
        //Edit layout cell
        let itemSize = UIScreen.main.bounds.width/3 - 3
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(20, 0, 10, 0)
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        layout.minimumLineSpacing = 3
        layout.minimumInteritemSpacing = 3
        
        collGallery.collectionViewLayout = layout
        
        // Do any additional setup after loading the view.
        collectImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @IBAction func btnDone(_ sender: Any) {
//
//    }
    @IBAction func btnDone(_ sender: Any) {
        performSegue(withIdentifier: "Detail_segue", sender: self)
    }
    
//    @IBAction func btnDone(_ sender: Any) {
//        performSegue(withIdentifier: "Detail_segue", sender: self)
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GalleryCollectionViewCell
        cell.imgViewCell.image = self.imageArr[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collGallery.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.blue.cgColor
        cell?.layer.borderWidth = 2
        
        self.imgSelectedArr.updateValue(self.imageArr[indexPath.item], forKey: indexPath.row)
        num += 1
        self.title = "\(num) Photos Selected"
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collGallery.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.blue.cgColor
        cell?.layer.borderWidth = 0
        
        self.imgSelectedArr.removeValue(forKey: indexPath.row)
        num -= 1
        self.title = "\(num) Photos Selected"
        if imgSelectedArr.count == 0 {
            self.title = "Photos"
        }
    }

    // MARK: Get image from system gallery
    func collectImage() {
        self.imageArr = []
        
        DispatchQueue.global(qos: .background).async {
            let imgManager = PHImageManager.default()
            
            //Collecting image from system gallery
            let requestOptions = PHImageRequestOptions()
            requestOptions.isSynchronous = true
            requestOptions.deliveryMode = .highQualityFormat
            
            //Order by Date
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
            
            //Append image to imageArray
            let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            print(fetchResult)
            print(fetchResult.count)
            if fetchResult.count > 0 {
                for i in 0..<fetchResult.count{
                    imgManager.requestImage(for: fetchResult.object(at: i) as PHAsset, targetSize: CGSize(width:500, height: 500),contentMode: .aspectFill, options: requestOptions, resultHandler: { (image, error) in
                        self.imageArr.append(image!)
                    })
                }
            } else {
                print("No photo.")
            }
            print("imageArray count: \(self.imageArr.count)")
            
            //Reload collection view
            DispatchQueue.main.async {
                print("Main queue")
                self.collGallery.reloadData()
            }
        }
    }
    
    //Transfer array, position to destination VC
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Detail_segue" {
            let dtVC = segue.destination as! DetailViewController
            dtVC.imgSelectedArr = self.imgSelectedArr
        }
    }
}

