//
//  ImportViewController.swift
//  SwiftTraining
//
//  Created by Thanh Lam on 1/29/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit
import CoreData
import Photos

class ImportViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfAge: UITextField!
    @IBOutlet weak var tfDescription: UITextField!
    @IBOutlet weak var sgmGender: UISegmentedControl!
    @IBOutlet weak var imgAvatar: UIImageView!
    
    var datePicker: UIDatePicker!
    var gender: String!
    var managedObjextContext: NSManagedObjectContext!
    var infos = [Info]()
    var details: Info? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        managedObjextContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        tfName.text = ""
        tfAge.text = ""
        sgmGender.selectedSegmentIndex = 0
        tfDescription.text = ""
        
        tfName.text = details?.name
        tfAge.text = details?.age
        tfDescription.text = details?.detail
        switch details?.gender {
        case "Male"?:
            sgmGender.selectedSegmentIndex = 0
        case "Female"?:
            sgmGender.selectedSegmentIndex = 1
        default:
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (details == nil) {
            print("is new")
            
            tfName.text = ""
            tfAge.text = ""
            sgmGender.selectedSegmentIndex = 0
            tfDescription.text = ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tfDatePicker(_ sender: UITextField) {
        self.pickUpDate(self.tfAge)
    }
    
    func pickUpDate(_ textField: UITextField) {
        // DatePicker
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        tfAge.text = dateFormatter1.string(from: datePicker.date)
        tfAge.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        tfAge.resignFirstResponder()
    }
    
    //MARK: user UIImagePickerController
    @IBAction func btnAddPhoto(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        //open system photoLibrary
        imagePickerController.sourceType = .photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func btCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Save data to coredata
    @available(iOS 10.0, *)
    @IBAction func btSave(_ sender: Any) {
        if gender == nil {
            gender = "Male"
        }
        
        if (details == nil) {
            let info = Info(context: managedObjextContext)
            info.name = tfName.text
            info.age = tfAge.text
            info.gender = gender
            info.detail = tfDescription.text
            
            do {
                try self.managedObjextContext.save()
                // Alert confirm
                let alert = UIAlertController(title: "Successful", message: "Info saved!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } catch {
                print("Failed saving")
            }
        } else {
            updateMember(name: tfName.text!, gender: gender, birthday: tfAge.text!, detail: tfDescription.text!, member: details!)
        }
        
        tfName.text = ""
        tfAge.text = ""
        tfDescription.text = ""
        sgmGender.selectedSegmentIndex = 0
    }
    
    // MARK: - Pick gender
    @IBAction func sgmGender(_ sender: Any) {
        switch sgmGender.selectedSegmentIndex {
        case 0:
            gender = "Male"
        case 1:
            gender = "Female"
        default:
            break
        }
    }
    
    func updateMember(name: String, gender: String, birthday: String, detail: String, member: Info){
        let person = member as NSManagedObject
        person.setValue(name, forKey: "name")
        person.setValue(gender, forKey: "gender")
        person.setValue(birthday, forKey: "age")
        person.setValue(detail, forKey: "detail")
        
        do{
            let alert = UIAlertController(title: "Successful", message: "Info updated!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            try person.managedObjectContext?.save()
        } catch{
            let saveError = error as NSError
            print(saveError)
        }
    }
    
    //MARK: choose image from system photoLibrary
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imgAvatar.image = image
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
