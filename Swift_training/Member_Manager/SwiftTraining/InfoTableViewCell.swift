//
//  InfoTableViewCell.swift
//  SwiftTraining
//
//  Created by Thanh Lam on 1/31/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbGender: UILabel!
    @IBOutlet weak var lbBirthDay: UILabel!
    @IBOutlet weak var lbDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
