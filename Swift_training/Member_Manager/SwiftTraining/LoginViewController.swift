//
//  LoginViewController.swift
//  SwiftTraining
//
//  Created by Thanh Lam on 1/29/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit

let defaults = UserDefaults.standard

class LoginViewController: UIViewController {

    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfPass: UITextField!
    @IBOutlet weak var lbError: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Login func
    @IBAction func btLogin(_ sender: Any) {
        let username: String = tfUserName.text!
        let password: String = tfPass.text!

//        defaults.setValue(username, forKey: "username")
//        defaults.setValue(password, forKey: "password")
        
        let userStored: String = (defaults.string(forKey: "username"))!
        let passStored: String = (defaults.string(forKey: "password"))!
        
        if ((username == userStored) && (password == passStored) && (username != "") && (password != "")){
            defaults.set(true, forKey: "LoggedIn")
            defaults.synchronize()
            lbError.isHidden = true
            tfUserName.text = ""
            tfPass.text = ""
            self.dismiss(animated: true, completion: nil)
        } else {
            print("error")
            lbError.isHidden = false
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
