//
//  MainViewController.swift
//  SwiftTraining
//
//  Created by Thanh Lam on 1/29/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tbInfo: UITableView!
    
    var infor = [Info]()
    var index: Int? = 0
    var managedObjextContext: NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbInfo.delegate = self
        tbInfo.dataSource = self
        managedObjextContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Check logged in
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let userIsLoggedIn = defaults.bool(forKey: "LoggedIn")
        OperationQueue.main.addOperation {
            if (userIsLoggedIn == false) {
                self.performSegue(withIdentifier: "next", sender: self)
            } else {
                self.reLoadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btAdd(_ sender: Any) {
//        defaults.set(true, forKey: "addNew")
        self.performSegue(withIdentifier: "detail", sender: true)
    }
    
    // MARK: - Logout func
    @IBAction func btLogout(_ sender: Any) {
        defaults.set(false, forKey: "LoggedIn")
        defaults.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infor.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let infoItem = infor[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! InfoTableViewCell
        
        cell.lbName.text = infoItem.name
        cell.lbBirthDay.text = infoItem.age
        cell.lbGender.text = infoItem.gender
        cell.lbDetail.text = infoItem.detail
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            //Remove data on table
            infor.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            //Remove data in coredata
            let fetchRequest: NSFetchRequest<Info> = Info.fetchRequest()
            do {
                let results = try managedObjextContext.fetch(fetchRequest)
                managedObjextContext.delete(results[indexPath.row])
                try self.managedObjextContext.save()
                reLoadData()
            } catch {
                print("Failed saving")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
//        defaults.set(false, forKey: "addNew")
        self.performSegue(withIdentifier: "detail", sender: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destViewController = segue.destination as! ImportViewController
        if (segue.identifier == "detail") {
        let isNew = sender as! Bool
        if isNew {
            destViewController.details = nil
        } else {
            destViewController.details = infor[index!]
            }
        }
    }
    
    // MARK: - Reload data on table
    func reLoadData(){
        let infoRequest: NSFetchRequest<Info> = Info.fetchRequest()
        
        do {
            infor = try managedObjextContext.fetch(infoRequest)
            tbInfo.reloadData()
        } catch {
            print("Could not load data from database \(error.localizedDescription)")
        }
    }

}
