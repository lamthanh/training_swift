//
//  Button.swift
//  Society_Login
//
//  Created by Thanh Lam on 2/2/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit

class Button: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: CGRect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
