//
//  ViewController.swift
//  Society_Login
//
//  Created by Thanh Lam on 2/2/18.
//  Copyright © 2018 Thanh Lam. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class ViewController: UIViewController {
    
    let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(FBSDKAccessToken.current())
        if FBSDKAccessToken.current() != nil {
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btFbLogin(_ sender: Any) {
        login()
    }
    
    func login() {
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, err) in
            if (err == nil) {
                print("Login successful")
            } else {
                print("Login fail")
            }
        }
    }
    
    func bt() {
        
    }
}

